
//Db functions
const uuid = require('uuid');
const sanitizer = require('validator');
const AWS = require('aws-sdk');
const DB = new AWS.DynamoDB.DocumentClient({});
const bcrypt = require('bcryptjs');

const UsersTableName = process.env.USERS_TABLE;
const IngredientsTableName = process.env.INGREDIENTS_TABLE;

/*** Users ****/
const getUserByEmail = async (email) => {
    return await DB.scan({
        TableName: UsersTableName,
        Key: {
            "email": { "S": sanitizer.normalizeEmail(sanitizer.trim(email)) }
        }
    }).promise().then(res => (res && res.Items && res.Items[0]) ? res.Items[0] : null).catch(err => null);
}
const getUserById = async (id) => {
    return await DB.get({
        TableName: UsersTableName,
        Key: { id: id }
    }).promise();
}

const saveUser = async () => {
    const email = 'sefasaiddeniz1@gmail.com';
    const password = 'testtest';
    const params = {
        TableName: UsersTableName,
        Item: {
            id: uuid.v1(),
            email: email,
            password: await bcrypt.hash(password, 8),
        },
    };
    return await DB.put(params).promise();
}

/*** Ingredients ****/

const getIngredients = async () => {
    return await DB.scan({
        TableName: IngredientsTableName
    }).promise().catch(err => null);
}

const getSingleIngredient = async (id) => {
    return await DB.get({
        TableName: IngredientsTableName,
        Key: { id: id }
    }).promise();
}

const addIngredient = async (data) => {

    const params = {
        TableName: IngredientsTableName,
        Item: {
            ...data,
            id: uuid.v1(),
        },
    };
    return await DB.put(params).promise();
}
const deleteIngredient = async (id) => {
    const params = {
        TableName: IngredientsTableName,
        Key: {
            id: id
        }
    }
    return await DB.delete(params).promise();
}
const updateIngredient = async (params, id) => {
    delete params.id;
    const updateParams = {
        TableName: IngredientsTableName,
        Key: {
            id: id
        },
        AttributeUpdates: {
            imageName: {
                Action: "PUT",
                Value: params.imageName
            },
            calories: {
                Action: "PUT",
                Value: params.calories
            },
            fat: {
                Action: "PUT",
                Value: params.fat
            },
            carbohydrates: {
                Action: "PUT",
                Value: params.carbohydrates
            },
            title: {
                Action: "PUT",
                Value: params.title
            },
            imageUrl: {
                Action: "PUT",
                Value: params.imageUrl
            }
        },
        ReturnValues: "UPDATED_NEW"
    }
    return await DB.update(updateParams).promise()
}


module.exports = {
    saveUser,
    getUserById,
    getUserByEmail,
    getIngredients,
    getSingleIngredient,
    addIngredient,
    deleteIngredient,
    updateIngredient
}
