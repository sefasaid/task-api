'use strict';
const helpers = require('./helpers');
const db = require('./db');
const PER_PAGE = 10;
const get = async (event) => {
  // This is not ideal but works in serverless offline
  if (!await helpers.verifyToken(event)) return helpers.formatReturn({ error: 'Not allowed' }, 403);
  const params = event.queryStringParameters;
  // LastEvaluatedKey is not enough for going back pages

  const ingredients = await db.getIngredients();

  let start = 0;
  if (!!params && !!params.page) {
    start = (params.page - 1) * PER_PAGE;
  }
  ingredients.Items = ingredients.Items.splice(start, PER_PAGE)
  if (!ingredients) return helpers.formatReturn({ error: 'Error' }, 501);
  return helpers.formatReturn({ ...ingredients, PerPage: PER_PAGE }, 200);
};
const getSingle = async (event) => {
  // This is not ideal but works in serverless offline
  if (!await helpers.verifyToken(event)) return helpers.formatReturn({ error: 'Not allowed' }, 403);
  const id = event.pathParameters.id;
  if (!id) return helpers.formatReturn({ error: 'Not Found' }, 404);
  const ingredient = await db.getSingleIngredient(id);
  if (!ingredient) return helpers.formatReturn({ error: 'Not Found' }, 404);
  return helpers.formatReturn(ingredient, 200);
};
const deleteSingle = async (event) => {

  if (!await helpers.verifyToken(event)) return helpers.formatReturn({ error: 'Not allowed' }, 403);
  const id = event.pathParameters.id;
  const ingredient = await db.deleteIngredient(id);
  if (!ingredient) return helpers.formatReturn({ error: 'Not Found' }, 404);
  return helpers.formatReturn(ingredient, 202);
}
const updateSingle = async (event) => {
  if (!await helpers.verifyToken(event)) return helpers.formatReturn({ error: 'Not allowed' }, 403);
  const id = event.pathParameters.id;
  const requestBody = JSON.parse(event.body);
  if (
    !requestBody.imageName ||
    !requestBody.title ||
    !requestBody.calories ||
    !requestBody.carbohydrates ||
    !requestBody.fat
  ) return helpers.formatReturn({ error: 'Not allowed' }, 401);
  requestBody.imageUrl = 'https://task-api-bucket.s3.amazonaws.com/' + requestBody.imageName;
  const ingredient = await db.updateIngredient(requestBody,id);
  if (!ingredient) return helpers.formatReturn({ error: 'Not Found' }, 404);
  return helpers.formatReturn(ingredient, 202);
}


const add = async (event) => {
  if (!await helpers.verifyToken(event)) return helpers.formatReturn({ error: 'Not allowed' }, 403);
  const requestBody = JSON.parse(event.body);
  if (
    !requestBody.imageName ||
    !requestBody.title ||
    !requestBody.calories ||
    !requestBody.carbohydrates ||
    !requestBody.fat
  ) return helpers.formatReturn({ error: 'Not allowed' }, 401);
  requestBody.imageUrl = 'https://task-api-bucket.s3.amazonaws.com/' + requestBody.imageName;
  const ingredient = await db.addIngredient(requestBody);
  if (!ingredient) return helpers.formatReturn({ error: 'Not Found' }, 404);
  return helpers.formatReturn({ ok: 'ok' }, 201);
}


module.exports = {
  get,
  getSingle,
  add,
  deleteSingle,
  updateSingle
}