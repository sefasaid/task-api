
const jwt = require('jsonwebtoken');
const db = require('./db');
const AWS = require('aws-sdk')
const uuid = require('uuid');

AWS.config.update({ region: 'us-east-1' })
const s3 = new AWS.S3({ signatureVersion: 'v4' })
const formatReturn = async (data, status) => {
    return {
        statusCode: status,
        headers: {
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Credentials': true,
        },
        body: JSON.stringify(
            data,
            null,
            2
        ),
    }
}
const verifyToken = async (event) => {
    let token = event.headers.Authorization || event.headers.authorization;
    if (!token) return false;
    if (token.startsWith('Bearer ')) {
        token = token.slice(7, token.length);
    }
    const decodedId = await jwt.verify(token, process.env.JWT_SECRET);
    if (!!decodedId.errorMessage) return false;
    const user = await db.getUserById(decodedId.id);
    if (user) return user;
    return false;
}


const getUploadURL = async function (event) {
    if (!await verifyToken(event)) return formatReturn({ error: 'Not allowed' }, 403);
    const requestBody = JSON.parse(event.body);
    const fileExt = requestBody.fileName.split('.').pop();
    console.log(fileExt);

    const actionId = uuid.v1();
    const Bucket = 'task-api-bucket';
    const s3Params = {
        Bucket,
        Key: `${ actionId }.${ fileExt }`,
        ContentType: `image/${ fileExt }`, // Update to match whichever content type you need to upload
        ACL: 'public-read',      // Enable this setting to make the object publicly readable - only works if the bucket can support public objects,
        Expires: 30000,
    }
    console.log(s3Params)
    return new Promise((resolve, reject) => {
        // Get signed URL
        resolve({
            "statusCode": 200,
            "isBase64Encoded": false,
            "headers": {
                "Access-Control-Allow-Origin": "*"
            },
            "body": JSON.stringify({
                "uploadURL": s3.getSignedUrl('putObject', s3Params),
                "photoFilename": `${ actionId }.${ fileExt }`
            })
        })
    })
}
module.exports = {
    formatReturn,
    verifyToken,
    getUploadURL
}