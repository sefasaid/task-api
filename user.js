'use strict';


const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const helper = require('./helpers');
const db = require('./db');
module.exports.login = async (event) => {
    const requestBody = JSON.parse(event.body);
    const email = requestBody.email;
    const password = requestBody.password;
    // if username or password is empty
    if (!email || !password) return helper.formatReturn({ error: 'Email/Password is not correct' }, 400);

    // Get User By Email
    const user = await db.getUserByEmail(email);
    // If User Not Found
    if (!user) return helper.formatReturn({ error: 'Email/Password is not correct' }, 400);

    // Check Password
    const passwordIsValid = await bcrypt.compare(password, user.password);

    // Password Not Match
    if (!passwordIsValid) return helper.formatReturn({ error: 'Email/Password is not correct' }, 400);

    // Return JWT
    return helper.formatReturn({ token: jwt.sign({ id: user.id }, process.env.JWT_SECRET, { expiresIn: 86400 }), ...user }, 200)
};
module.exports.init = async (event) => {
    await db.saveUser();
    return helper.formatReturn({ 'ok': 'ok' }, 200)
};